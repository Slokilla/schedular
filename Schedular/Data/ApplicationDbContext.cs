﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Schedular.Models;

namespace Schedular.Data
{
    public class ApplicationDbContext : IdentityDbContext
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }
        public Microsoft.EntityFrameworkCore.DbSet<Schedular.Models.Ue> Ue { get; set; }
        public Microsoft.EntityFrameworkCore.DbSet<Schedular.Models.Batiment> Batiment { get; set; }
        public Microsoft.EntityFrameworkCore.DbSet<Schedular.Models.Groupe> Groupe { get; set; }
        public Microsoft.EntityFrameworkCore.DbSet<Schedular.Models.Salle> Salle { get; set; }
        public Microsoft.EntityFrameworkCore.DbSet<Schedular.Models.Seance> Seance { get; set; }
        public Microsoft.EntityFrameworkCore.DbSet<Schedular.Models.SalleSeance> SalleSeance{get;set;}
        //ModelBuilder.Entity<Ue>().HasMany(m => m.Members).WithOptional().HasForeignKey(m => m.CompanyId).WillCascadeOnDelete(true);


        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {

            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<Seance>()
               .HasOne(s => s.leGroupe)
               .WithMany(g => g.lesSeances)
               .OnDelete(DeleteBehavior.SetNull);

            modelBuilder.Entity<Seance>()
               .HasOne(s => s.lUe)
               .WithMany(g => g.lesSeances)
               .OnDelete(DeleteBehavior.Cascade);

            modelBuilder.Entity<Groupe>()
                .HasOne(g => g.Ue)
                .WithMany(u => u.lesGroupes)
                .OnDelete(DeleteBehavior.Restrict);

            modelBuilder.Entity<Ue>().HasMany(u => u.lesGroupes).WithOne(g => g.Ue).OnDelete(DeleteBehavior.Cascade);
            modelBuilder.Entity<Ue>().HasMany(u => u.lesSeances).WithOne(g => g.lUe).OnDelete(DeleteBehavior.Restrict);

            //Gestion du many to many entre séances et salles
            modelBuilder.Entity<SalleSeance>().HasKey(ss => new { ss.SalleID, ss.SeanceID });
            modelBuilder.Entity<SalleSeance>().HasOne(ss => ss.seance).WithMany(s => s.salleSeances).HasForeignKey(s => s.SeanceID);
            modelBuilder.Entity<SalleSeance>().HasOne(ss => ss.salle).WithMany(s => s.salleSeances).HasForeignKey(s => s.SalleID);
        }
    }
}
