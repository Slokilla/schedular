﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Schedular.Models;

namespace Schedular.Pages
{
    public class IndexModel : PageModel
    {
        private readonly ILogger<IndexModel> _logger;
        private readonly Schedular.Data.ApplicationDbContext _context;

        //public IndexModel(ILogger<IndexModel> logger)
        public IndexModel(Schedular.Data.ApplicationDbContext context)
        {
            _context = context;
        }

        public IList<Seance> Seance { get; set; }

        public async Task OnGetAsync()
        {
            //Seance = await _context.Salle.Include(e => e.leBatiment).FirstOrDefaultAsync(m => m.ID == id);
            Seance = await _context.Seance.Include(e => e.salleSeances).Include(e => e.leGroupe).Include(e => e.lUe).ToListAsync();
        }
    }
}
