﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using Schedular.Data;
using Schedular.Models;

namespace Schedular.Pages.Salles
{
    [Authorize(Roles = "Gestionnaires")]
    public class DeleteModel : PageModel
    {
        private readonly Schedular.Data.ApplicationDbContext _context;

        public DeleteModel(Schedular.Data.ApplicationDbContext context)
        {
            _context = context;
        }

        [BindProperty]
        public Salle Salle { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }


            Salle = await _context.Salle.Include(e => e.leBatiment).FirstOrDefaultAsync(m => m.ID == id);


            if (Salle == null)
            {
                return NotFound();
            }

            Salle = await _context.Salle.Include(e => e.leBatiment).FirstOrDefaultAsync(m => m.ID == id);

            return Page();
        }

        public async Task<IActionResult> OnPostAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Salle = await _context.Salle.FindAsync(id);

            if (Salle != null)
            {
                _context.Salle.Remove(Salle);
                await _context.SaveChangesAsync();
            }

            return RedirectToPage("./Index");
        }
    }
}
