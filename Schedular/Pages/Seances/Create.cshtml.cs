﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Schedular.Data;
using Schedular.Models;

namespace Schedular.Pages.Seances
{
    [Authorize(Roles = "Gestionnaires")]
    public class CreateModel : SalleSeancePageModel
    {
        private readonly Schedular.Data.ApplicationDbContext _context;

        public CreateModel(Schedular.Data.ApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<IActionResult> OnGetAsync()
        {
            //Seance vide pour décocher toutes les cases
            var seance = new Seance();
            seance.salleSeances = new List<SalleSeance>();

            //Récupération des salles et des batiments auxquels elles sont liées. 
            foreach (Salle salle in _context.Salle)
            {
                Salle = await _context.Salle.Include(e => e.leBatiment).FirstOrDefaultAsync(m => m.ID == salle.ID);
            }

            AddSalleSeance(_context, seance);
            

            ViewData["ueID"] = new SelectList(_context.Ue, "ID", "intitule");
            ViewData["groupeID"] = new SelectList(_context.Groupe, "ID", "nom");

            return Page();
        }

        [BindProperty]
        public Seance Seance { get; set; }
        public Salle Salle { get; private set; }

        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        public async Task<IActionResult> OnPostAsync(string[] selectedSalle)
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }


            var newSeance = new Seance(); 
            if (selectedSalle != null)
            {
                newSeance.salleSeances = new List<SalleSeance>();

                foreach (var salle in selectedSalle)
                {
                    var newSalleSeance = new SalleSeance
                    {
                        SalleID = int.Parse(salle)
                    };
                    newSeance.salleSeances.Add(newSalleSeance);
                }

            }

            if (await TryUpdateModelAsync<Seance>(
                newSeance,
                "Seance",
                i => i.date, 
                i => i.heureDebut, 
                i => i.heureFin, 
                i => i.groupeID, 
                i => i.ueID, 
                i => i.type))
            {
                _context.Seance.Add(newSeance);
                await _context.SaveChangesAsync();
                return RedirectToPage("./Index");
            }

            AddSalleSeance(_context, newSeance);

            return RedirectToPage("./Index");
        }
    }
}
