﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using Schedular.Data;
using Schedular.Models;

namespace Schedular.Pages.Seances
{
    [Authorize(Roles = "Gestionnaires")]
    public class IndexModel : PageModel
    {
        private readonly Schedular.Data.ApplicationDbContext _context;

        public IndexModel(Schedular.Data.ApplicationDbContext context)
        {
            _context = context;
        }

        public IList<Seance> Seance { get;set; }
        public IList<Salle> Salle{ get; set; }

        public async Task OnGetAsync()
        {
            //Seance = await _context.Seance.Include(e => e.lUe).Include(e => e.leGroupe).Include(e => e.salleSeances).ToListAsync();
            //foreach(Seance seance ss in Seance)

            Seance = await _context.Seance.Include(e => e.lUe).Include(e => e.leGroupe).Include(s => s.salleSeances).ThenInclude(ss => ss.salle).ThenInclude(s => s.leBatiment).AsNoTracking().ToListAsync();
        }
    }
}
