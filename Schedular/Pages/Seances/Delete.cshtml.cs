﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using Schedular.Data;
using Schedular.Models;

namespace Schedular.Pages.Seances
{
    [Authorize(Roles = "Gestionnaires")]
    public class DeleteModel : PageModel
    {
        private readonly Schedular.Data.ApplicationDbContext _context;

        public DeleteModel(Schedular.Data.ApplicationDbContext context)
        {
            _context = context;
        }

        [BindProperty]
        public Seance Seance { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            //Seance = await _context.Seance.Include(e => e.lUe).Include(e => e.leGroupe).Include(e => e.salleSeances).FirstOrDefaultAsync(m => m.ID == id);
            Seance = await _context.Seance.Include(e => e.lUe).Include(e => e.leGroupe).Include(s => s.salleSeances).ThenInclude(ss => ss.salle).ThenInclude(s => s.leBatiment).AsNoTracking().FirstOrDefaultAsync(m => m.ID == id);

            if (Seance == null)
            {
                return NotFound();
            }


            return Page();
        }

        public async Task<IActionResult> OnPostAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Seance = await _context.Seance.FindAsync(id);

            if (Seance != null)
            {
                _context.Seance.Remove(Seance);
                await _context.SaveChangesAsync();
            }

            return RedirectToPage("./Index");
        }
    }
}
