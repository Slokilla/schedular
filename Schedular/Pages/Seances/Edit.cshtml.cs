﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Schedular.Data;
using Schedular.Models;

namespace Schedular.Pages.Seances
{
    [Authorize(Roles = "Gestionnaires")]
    public class EditModel : SalleSeancePageModel
    {
        private readonly Schedular.Data.ApplicationDbContext _context;

        public EditModel(Schedular.Data.ApplicationDbContext context)
        {
            _context = context;
        }

        [BindProperty]
        public Seance Seance { get; set; }
        public Salle Salle { get; private set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Seance = await _context.Seance.Include(s => s.salleSeances).ThenInclude(ss => ss.salle).AsNoTracking().FirstOrDefaultAsync(m => m.ID == id);
            
            if (Seance == null)
            {
                return NotFound();
            }

            ViewData["ueID"] = new SelectList(_context.Ue, "ID", "intitule");
            ViewData["groupeID"] = new SelectList(_context.Groupe, "ID", "nom");

            //Récupération des salles et des batiments auxquels elles sont liées. 
            foreach (Salle salle in _context.Salle)
            {
                Salle = await _context.Salle.Include(e => e.leBatiment).FirstOrDefaultAsync(m => m.ID == salle.ID);
            }
           
            AddSalleSeance(_context, Seance);
            return Page();
        }

        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        public async Task<IActionResult> OnPostAsync(int? id, string[] selectedSalle)
        {
            if (!ModelState.IsValid)
            {
                ViewData["salleID"] = new SelectList(_context.Salle, "ID", "nom");
                ViewData["ueID"] = new SelectList(_context.Ue, "ID", "intitule");
                ViewData["groupeID"] = new SelectList(_context.Groupe, "ID", "nom");
                return Page();
            }
            ////////////////////////////////////
            //Si c'est ok
            var SeanceAModifier = await _context.Seance
                .Include(i => i.salleSeances)
                .ThenInclude(i => i.salle)
                .FirstOrDefaultAsync(s => s.ID == id);

            if (await TryUpdateModelAsync<Seance>(
                SeanceAModifier,
                "Seance",
                i => i.date,
                i => i.heureDebut,
                i => i.heureFin,
                i => i.groupeID,
                i => i.ueID,
                i => i.type))
            {
                UpdateSalleSeance(_context, selectedSalle, SeanceAModifier);
                await _context.SaveChangesAsync();
                return RedirectToPage("./Index");
            }

            UpdateSalleSeance(_context, selectedSalle, SeanceAModifier);

            AddSalleSeance(_context, SeanceAModifier);
            return RedirectToPage("./Index");
        }
    }
}
