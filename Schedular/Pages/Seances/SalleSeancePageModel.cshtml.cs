using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Schedular.Data;
using Schedular.Models;
using Schedular.Models.ViewModels;

namespace Schedular.Pages.Seances
{
    public class SalleSeancePageModel : PageModel
    {
        public List<CheckSalleSeance> lesCheckSalleSeance;

        public void AddSalleSeance(ApplicationDbContext context, Seance seance)
        {
            var lesSalles = context.Salle;
            var SallesSeances= new HashSet<int>(seance.salleSeances.Select(ss => ss.SalleID));

            lesCheckSalleSeance = new List<CheckSalleSeance>();

            foreach(var salle in lesSalles)
            {
                lesCheckSalleSeance.Add(new CheckSalleSeance
                {
                    SalleID = salle.ID,
                    NomPrecis = salle.nomPrecis,
                    IsCheck = SallesSeances.Contains(salle.ID)
                }); 
               
                
            }

        }

        public void UpdateSalleSeance(ApplicationDbContext context,
            string[] selectedSalle, Seance seanceAModifier)
        {
            if (selectedSalle == null)
            {
                seanceAModifier.salleSeances = new List<SalleSeance>();
                return;
            }

            var selectedSalleHS = new HashSet<string>(selectedSalle);
            var salleSeance = new HashSet<int>
                (seanceAModifier.salleSeances.Select(c => c.salle.ID));
            foreach (var salle in context.Salle)
            {
                if (selectedSalleHS.Contains(salle.ID.ToString()))
                {
                    if (!salleSeance.Contains(salle.ID))
                    {
                        seanceAModifier.salleSeances.Add(
                            new SalleSeance
                            {
                                SeanceID= seanceAModifier.ID,
                                SalleID = salle.ID
                            });
                    }
                }
                else
                {
                    if (salleSeance.Contains(salle.ID))
                    {
                        SalleSeance enseigneAEnlever
                            = seanceAModifier
                                .salleSeances
                                .SingleOrDefault(i => i.SalleID == salle.ID);
                        context.Remove(enseigneAEnlever);
                    }
                }
            }
        }


    }
}
