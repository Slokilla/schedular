﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Schedular.Data;
using Schedular.Models;

namespace Schedular.Pages.Seances
{
    [Authorize(Roles = "Gestionnaires")]
    public class DetailsModel : PageModel
    {
        private readonly Schedular.Data.ApplicationDbContext _context;

        public DetailsModel(Schedular.Data.ApplicationDbContext context)
        {
            _context = context;
        }

        public Seance Seance { get; set; }
        public IList<SalleSeance> SalleSeance { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Seance = await _context.Seance.Include(e => e.lUe).Include(e => e.leGroupe).Include(s => s.salleSeances).ThenInclude(ss => ss.salle).ThenInclude(s => s.leBatiment).AsNoTracking().FirstOrDefaultAsync(m => m.ID == id);

            if (Seance == null)
            {
                return NotFound();
            }

            


            return Page();
        }
    }
}
