﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Schedular.Data;
using Schedular.Models;

namespace Schedular.Pages.Ues
{
    [Authorize(Roles = "Gestionnaires")]
    public class EditModel : PageModel
    {
        private readonly Schedular.Data.ApplicationDbContext _context;

        public EditModel(Schedular.Data.ApplicationDbContext context)
        {
            _context = context;
        }

        [BindProperty]
        public Ue Ue { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Ue = await _context.Ue.FirstOrDefaultAsync(m => m.ID == id);

            if (Ue == null)
            {
                return NotFound();
            }
            return Page();
        }

        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            _context.Attach(Ue).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!UeExists(Ue.ID))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return RedirectToPage("./Index");
        }

        private bool UeExists(int id)
        {
            return _context.Ue.Any(e => e.ID == id);
        }
    }
}
