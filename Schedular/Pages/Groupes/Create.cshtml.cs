﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Schedular.Data;
using Schedular.Models;

namespace Schedular.Pages.Groupes
{
    [Authorize(Roles = "Gestionnaires")]
    public class CreateModel : PageModel
    {
        private readonly Schedular.Data.ApplicationDbContext _context;

        public CreateModel(Schedular.Data.ApplicationDbContext context)
        {
            _context = context;
        }

        public IActionResult OnGet()
        {
            ViewData["UeID"] = new SelectList(_context.Ue, "ID", "intitule");

            return Page();
        }

        [BindProperty]
        public Groupe Groupe { get; set; }

        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            _context.Groupe.Add(Groupe);
            await _context.SaveChangesAsync();

            return RedirectToPage("./Index");
        }
    }
}
