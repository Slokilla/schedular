﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using Schedular.Data;
using Schedular.Models;

namespace Schedular.Pages.Groupes
{
    [Authorize(Roles = "Gestionnaires")]
    public class IndexModel : PageModel
    {
        private readonly Schedular.Data.ApplicationDbContext _context;

        public IndexModel(Schedular.Data.ApplicationDbContext context)
        {
            _context = context;
        }

        public IList<Groupe> Groupe { get;set; }

        public async Task OnGetAsync()
        {
            Groupe = await _context.Groupe.Include(e => e.Ue).ToListAsync();
        }
    }
}
