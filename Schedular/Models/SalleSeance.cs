﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Schedular.Models
{
    public class SalleSeance
    {
        public int SalleID { get; set; }
        public Salle salle { get; set; }
        public int SeanceID { get; set; }
        public Seance seance { get; set; }
    }
}
