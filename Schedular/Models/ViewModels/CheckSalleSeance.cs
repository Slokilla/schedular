﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Schedular.Models.ViewModels
{
    public class CheckSalleSeance
    {
        public int SalleID { get; set; }
        public string NomPrecis { get; set; }
        public bool IsCheck { get; set; }

    }
}
