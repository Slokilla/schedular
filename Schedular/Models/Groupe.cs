﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Schedular.Models
{
    public class Groupe
    {
        public Int32 ID { get; set; }
        [Display(Name = "Nom")]
        public String nom { get; set; }
        [Display(Name = "Ue liée")]
        public Int32 ueID { get; set; } 

        //Lien de navigation
        //public Ue lUe { get; set; }
        public ICollection<Seance> lesSeances { get; set; }
        public Ue Ue { get; set; }
    }
}
