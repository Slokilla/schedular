﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ComponentModel.DataAnnotations;
using System.Threading.Tasks;

namespace Schedular.Models
{
    public class Salle
    {
        public Int32 ID { get; set; }
        [Required]
        [Display(Name = "Bâtiment")]
        public Int32 batimentID { get; set; }
        [Required]
        [Display(Name = "Nom")]
        public String nom { get; set; }

        //Lien de navigation 
        public Batiment leBatiment { get; set; }
        public ICollection<SalleSeance> salleSeances {get;set;}

        //Non persistant 
        [Display(Name = "Salle - Bâtiment")]
        public string nomPrecis
        {
            get
            {
                if (leBatiment != null)
                    return nom + " - " + leBatiment.nom;
                else
                    return "bugged";
            }
        }
    }
}
