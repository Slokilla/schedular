﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Schedular.Models
{
    public class Batiment
    {
        public Int32 ID { get; set; }
        [Display(Name = "Nom")]
        public String nom { get; set; }

        //Lien de navigation 
        public ICollection<Salle> lesSalles { get; set; }
    }
}
