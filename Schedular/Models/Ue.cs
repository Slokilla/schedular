﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Schedular.Models
{
    public class Ue
    {
        public Int32 ID {get; set; }
        [Display(Name = "Code d'identification")]
        public String numero {get; set; }
        [Display(Name = "Intitulé")]
        public String intitule {get; set; }

        //Lien de Navigation
        public ICollection<Seance> lesSeances { get; set; }
        public ICollection<Groupe> lesGroupes { get; set; }
    }
}
