﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ComponentModel.DataAnnotations;
using System.Threading.Tasks;

namespace Schedular.Models
{
    public class Seance
    {
        //public enum EnumType
        //{
        //    CM, TD, TP
        //}
        ////@TODO essayer de bien faire l'affichage pour choisir seulement l'heure
        public Int32 ID { get; set; }
        [Required]
        [DataType(DataType.Date)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:yyyy-MM-dd}")]
        [Display(Name = "Date")]
        public DateTime date { get; set; }
        [Required]
        [Display(Name = "Début")]
        [DataType(DataType.Time)]
        public String heureDebut { get; set; }
        [Required]
        [DataType(DataType.Time)]
        [Display(Name = "Fin")]
        public String heureFin { get; set; }
        [Display(Name = "Groupe")]
        public Int32? groupeID { get; set; }
        [Display(Name = "UE")]
        public Int32? ueID { get; set; }
        //[Required]
        //[Display(Name = "Salle")]
        //public Int32 salleID { get; set; }
        [Required]
        [Display(Name = "Type")]
        public TypeSeance.EnumType type { get; set; }
        
        //Liens de navigation 
        public Groupe leGroupe { get; set; }
        public Ue lUe { get; set; }
        public ICollection<SalleSeance> salleSeances { get; set; }

    }
}
